package com.example.huunghia.chatfirebase.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.example.huunghia.chatfirebase.R;
import com.example.huunghia.chatfirebase.model.User;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class UsersActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private ArrayList<User> listUser;
    private DatabaseReference databaseUser;
    private ListView lvUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_users);

        toolbar = findViewById(R.id.users_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("All Users");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //list view
        lvUser = findViewById(R.id.listUser);
        listUser = new ArrayList<>();
        lvUser.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                User currentUser = (User) lvUser.getItemAtPosition(i);
                String uid = currentUser.getUid();

                Intent profileIntent = new Intent(UsersActivity.this, ProfileActivity.class);
                profileIntent.putExtra("user_id", uid);
                startActivity(profileIntent);

            }
        });

        databaseUser = FirebaseDatabase.getInstance().getReference().child("Users");
    }

    @Override
    protected void onStart() {
        super.onStart();

        databaseUser.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                listUser.clear();

                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {

                    User userObj = postSnapshot.getValue(User.class);

                    listUser.add(userObj);
                }

                ArrayAdapter<User> adapter = new ArrayAdapter<>(UsersActivity.this,
                        android.R.layout.simple_list_item_1, listUser);
                lvUser.setAdapter(adapter);

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
}
