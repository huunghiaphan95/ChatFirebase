package com.example.huunghia.chatfirebase.activity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.huunghia.chatfirebase.R;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.DateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class ProfileActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private TextView tvUserName, tvUserEmail;
    private Button btnKetBan;
    private DatabaseReference currentUserDatabase, mFriendReqDatabase, mRootRef, mFriendDatabase;
    private String mCurrent_state;
    private FirebaseUser mCurrent_user;
    private ProgressDialog mProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        initId();

        mCurrent_state = "not_friends";

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Profile");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setTitle("Loading User Data");
        mProgressDialog.setMessage("Please wait while we load the user data.");
        mProgressDialog.setCanceledOnTouchOutside(false);
        mProgressDialog.show();

        final String user_id = getIntent().getStringExtra("user_id");

        currentUserDatabase = FirebaseDatabase.getInstance().getReference().child("Users").child(user_id);
        mFriendReqDatabase = FirebaseDatabase.getInstance().getReference().child("Friend_req");
        mFriendDatabase = FirebaseDatabase.getInstance().getReference().child("Friends");
        mRootRef = FirebaseDatabase.getInstance().getReference();

        mCurrent_user = FirebaseAuth.getInstance().getCurrentUser();

        currentUserDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                String name = dataSnapshot.child("username").getValue().toString();
                String email = dataSnapshot.child("email").getValue().toString();

                tvUserEmail.setText(email);
                tvUserName.setText(name);

                if(mCurrent_user.getUid().equals(user_id)){

                    //mDeclineBtn.setEnabled(false);
                    //mDeclineBtn.setVisibility(View.INVISIBLE);

                    btnKetBan.setEnabled(false);
                    btnKetBan.setVisibility(View.INVISIBLE);

                }

                mFriendReqDatabase.child(mCurrent_user.getUid())
                        .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {

                        if(dataSnapshot.hasChild(user_id)){

                            String req_type = dataSnapshot.child(user_id)
                                    .child("request_type").getValue().toString();

                            if(req_type.equals("received")){

                                mCurrent_state = "req_received";
                                btnKetBan.setText("dong y");

                                //mDeclineBtn.setVisibility(View.VISIBLE);
                                //mDeclineBtn.setEnabled(true);


                            } else if(req_type.equals("sent")) {

                                mCurrent_state = "req_sent";
                                btnKetBan.setText("huy yeu cau");

                                //mDeclineBtn.setVisibility(View.INVISIBLE);
                                //mDeclineBtn.setEnabled(false);

                            }

                            mProgressDialog.dismiss();

                        }else {
                            mFriendDatabase.child(mCurrent_user.getUid())
                                    .addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {

                                    if(dataSnapshot.hasChild(user_id)){

                                        mCurrent_state = "friends";
                                        btnKetBan.setText("huy ket ban");

                                        //mDeclineBtn.setVisibility(View.INVISIBLE);
                                        //mDeclineBtn.setEnabled(false);

                                    }

                                    mProgressDialog.dismiss();
                                }

                                @Override
                                public void onCancelled(DatabaseError databaseError) {

                                    mProgressDialog.dismiss();
                                }
                            });
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        btnKetBan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                btnKetBan.setEnabled(false);

                if(mCurrent_state.equals("not_friends")){

                    Map requestMap = new HashMap();
                    requestMap.put("Friend_req/" + mCurrent_user.getUid()
                            + "/" + user_id + "/request_type", "sent");
                    requestMap.put("Friend_req/" + user_id
                            + "/" + mCurrent_user.getUid() + "/request_type", "received");

                    mRootRef.updateChildren(requestMap, new DatabaseReference.CompletionListener() {
                        @Override
                        public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {

                            if(databaseError != null){

                                Toast.makeText(ProfileActivity.this,
                                        "There was some error in sending request", Toast.LENGTH_SHORT)
                                        .show();

                            } else {

                                mCurrent_state = "req_sent";
                                btnKetBan.setText("Huy yeu cau");

                            }

                            btnKetBan.setEnabled(true);

                        }
                    });
                }

                if(mCurrent_state.equals("req_sent")){

                    mFriendReqDatabase.child(mCurrent_user.getUid()).child(user_id).removeValue()
                            .addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {

                            mFriendReqDatabase.child(user_id).child(mCurrent_user.getUid()).removeValue().addOnSuccessListener(new OnSuccessListener<Void>() {
                                @Override
                                public void onSuccess(Void aVoid) {


                                    btnKetBan.setEnabled(true);
                                    mCurrent_state = "not_friends";
                                    btnKetBan.setText("ket ban");

                                    //mDeclineBtn.setVisibility(View.INVISIBLE);
                                    //mDeclineBtn.setEnabled(false);

                                }
                            });

                        }
                    });
                }

                if(mCurrent_state.equals("req_received")){

                    final String currentDate = DateFormat.getDateTimeInstance().format(new Date());

                    Map friendsMap = new HashMap();
                    friendsMap.put("Friends/" + mCurrent_user.getUid() + "/" + user_id + "/date", currentDate);
                    friendsMap.put("Friends/" + user_id + "/"  + mCurrent_user.getUid() + "/date", currentDate);


                    friendsMap.put("Friend_req/" + mCurrent_user.getUid() + "/" + user_id, null);
                    friendsMap.put("Friend_req/" + user_id + "/" + mCurrent_user.getUid(), null);


                    mRootRef.updateChildren(friendsMap, new DatabaseReference.CompletionListener() {
                        @Override
                        public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {

                            if(databaseError == null){

                                btnKetBan.setEnabled(true);
                                mCurrent_state = "friends";
                                btnKetBan.setText("huy ket ban");

                                //mDeclineBtn.setVisibility(View.INVISIBLE);
                                //mDeclineBtn.setEnabled(false);

                            } else {

                                String error = databaseError.getMessage();

                                Toast.makeText(ProfileActivity.this, error, Toast.LENGTH_SHORT).show();

                            }

                        }
                    });

                }

                //------------ UNFRIENDS ---------

                if(mCurrent_state.equals("friends")){

                    Map unfriendMap = new HashMap();
                    unfriendMap.put("Friends/" + mCurrent_user.getUid() + "/" + user_id, null);
                    unfriendMap.put("Friends/" + user_id + "/" + mCurrent_user.getUid(), null);

                    mRootRef.updateChildren(unfriendMap, new DatabaseReference.CompletionListener() {
                        @Override
                        public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {


                            if(databaseError == null){

                                mCurrent_state = "not_friends";
                                btnKetBan.setText("ket ban");

                                //mDeclineBtn.setVisibility(View.INVISIBLE);
                                //mDeclineBtn.setEnabled(false);

                            } else {

                                String error = databaseError.getMessage();

                                Toast.makeText(ProfileActivity.this, error, Toast.LENGTH_SHORT).show();
                            }

                            btnKetBan.setEnabled(true);

                        }
                    });

                }

            }
        });

    }

    private void initId(){
        toolbar = findViewById(R.id.profile_toolbar);
        tvUserEmail = findViewById(R.id.tvUserEmail);
        tvUserName = findViewById(R.id.tvUserName);
        btnKetBan = findViewById(R.id.btnKetBan);
    }
}
