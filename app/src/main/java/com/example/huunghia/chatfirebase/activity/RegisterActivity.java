package com.example.huunghia.chatfirebase.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.huunghia.chatfirebase.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;

public class RegisterActivity extends AppCompatActivity {

    //widget
    private EditText edtName, edtEmail, edtPassword;
    private Button btnXacNhan;
    private Toolbar toolbar;

    //firebase
    private FirebaseAuth mAuth;

    //ProgessDialog
    private ProgressDialog mResProgess;

    //database
    private DatabaseReference mDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        //anh xa id
        initId();

        //settting app bar
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Dang ky tai khoan");

        //auth
        mAuth = FirebaseAuth.getInstance();

        //progess dialog
        mResProgess = new ProgressDialog(this);

        //click button
        btnXacNhan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String name     = edtName.getText().toString().trim();
                String email    = edtEmail.getText().toString().trim();
                String password = edtPassword.getText().toString().trim();

                if (TextUtils.isEmpty(name)){
                    Toast.makeText(RegisterActivity.this,
                            "ten khong duoc rong", Toast.LENGTH_LONG).show();
                }else if (TextUtils.isEmpty(email)){
                    Toast.makeText(RegisterActivity.this,
                            "email khong duoc rong", Toast.LENGTH_LONG).show();
                }else if (TextUtils.isEmpty(password)){
                    Toast.makeText(RegisterActivity.this,
                            "password khong duoc rong", Toast.LENGTH_LONG).show();
                }else {
                    mResProgess.setTitle("Dang ky tai khoan");
                    mResProgess.setMessage("Dang tao tai khoan");
                    mResProgess.setCanceledOnTouchOutside(false);
                    mResProgess.show();

                    registerUser(name, email, password);
                }

            }
        });
    }

    //creat accout
    private void registerUser(final String name, final String email, String password) {
        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()){

                    FirebaseUser currentUser = mAuth.getCurrentUser();
                    String uid = currentUser.getUid();

                    mDatabase = FirebaseDatabase.getInstance().getReference().child("Users").child(uid);

                    HashMap<String, String> userMap = new HashMap<>();
                    userMap.put("email", email);
                    userMap.put("username", name);
                    userMap.put("uid", uid);

                    mDatabase.setValue(userMap).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if (task.isSuccessful()){
                                mResProgess.dismiss();

                                Intent mainIntent = new Intent(RegisterActivity.this, MainActivity.class);
                                startActivity(mainIntent);
                            }
                        }
                    });

                }else {
                    mResProgess.hide();

                    Toast.makeText(RegisterActivity.this,
                            "Tai khoan da ton tai", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    private void initId(){
        edtEmail    = findViewById(R.id.edtEmail);
        edtName     = findViewById(R.id.edtName);
        edtPassword = findViewById(R.id.edtPassword);

        btnXacNhan = findViewById(R.id.btnXacNhan);

        toolbar = findViewById(R.id.register_toolbar);
    }
}
